#!/usr/bin/Rscript --slave

# This script will merge snp-results with markerinfo and write results to file

# NB! Set working directory to your results folder.

# stop("error message to display") # Convention to have in funcions if failure. 

####### INSERT FILE NAMES ##################################
all.resfile.in <- "AllSNPs_kgfett.run"
sign.resfile.in <- "signSNPs_kgfett.run"
markerinfo.file.in <- "~/Projects/Geno_bovine/dmu/DMU-data/777k_markerinfo.txt"
allres.out <- "kgfett_all_results.txt"
signfile.out <- "kgfett_sign_results.txt"
############################################################


# Read files --------------------------------------------------------------
tab5rows <- read.table(all.resfile.in, header = TRUE, nrows = 5) # read first five rows
classes <- sapply(tab5rows, class) # save col.calsses for use with faster read.tabel performance
all.SNPs <- read.table(all.resfile.in, header=TRUE, comment.char="")
sign.SNPs <- read.table(sign.resfile.in, header=TRUE, comment.char="")
names(all.SNPs)[c(1,10)] <- c("name","P") 
names(sign.SNPs)[c(1,10)] <- c("name","P")


markers <- read.table(markerinfo.file.in, col.names=c("name","nr","SNP","BP","A1","A2","CHR"))
markers <- subset(markers, select =-nr) # delete the "nr" column.

# Merge objects -------------------------------------------------------------
all.merged <- merge(all.SNPs, markers)
sign.merged <- merge(sign.SNPs, markers)

# Subset objects and sort on P for sign ---------------------------------------------------------
manhattan.subset.all <- subset(all.merged, select = c(SNP, CHR, BP, P, chi2, fraction_of_variance_explained, A1, A2))
manhattan.subset.sign <- subset(sign.merged, select = c(SNP, CHR, BP, P, chi2, fraction_of_variance_explained, A1, A2))
manhattan.subset.sign <- with (manhattan.subset.sign, manhattan.subset.sign[order(P),])
manhattan.subset.all.sort_P <- with (manhattan.subset.all, manhattan.subset.all[order(P),])

# # manhattan plot ----------------------------------------------------------
# rewrite this so it is self contained. Use: paste(strsplit(all.merged, ".")[1], "manhattan) or something like it.)

download.file("https://raw.github.com/stephenturner/qqman/master/qqman.r", destfile="./qqman.r", method="wget")
source("./qqman.r") # source the script containing the functions.
png("manhattan_kgfett.png", width = 1000, height=700) # open plotting device
manhattan(subset(manhattan.subset.all, fraction_of_variance_explained >= 0.00001), main = "kgfett Manhattan plot")
### For use where infile exists

# manhattan(subset(all.SNPs, fraction_of_variance_explained >= 0.00001), pt.col=c("black","grey50","darkorange1"),
# pch=20, genomewideline=F, suggestiveline=F, main = "Manhattan plot NR1")
dev.off() # close plotting device and write to plot file.

# Write res files ---------------------------------------------------------
write.table(manhattan.subset.all, file=allres.out, row.names=FALSE, sep="\t")
write.table(manhattan.subset.sign, file=signfile.out, row.names=FALSE, sep="\t")

Sys.sleep(2)
rm(list=ls()) # clear memory.
if (file.exists("qqman.r")) file.remove("qqman.r") # removes qqman-function file
